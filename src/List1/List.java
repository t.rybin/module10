package List1;

class List {
    private int[] list;
    private int count;
    public List() {
        count = 0;
    }
    public void add(int item) {
        int[] temp;
        temp = list;
        list = new int[count + 1];
        for (int i = 0; i < count; i++) {
            list[i] = temp[i];
        }
        list[count] = item;
        count++;
    }
    public void insert(int item, int index) {
        if (index < 0 || index > count) {
            throw new RuntimeException("Queue is clear.");
        }
        if (index == count) {
            add(item);
            return;
        }
        int[] temp;
        temp = list;
        list = new int[count + 1];
        for (int i = 0; i < index; i++) {
            list[i] = temp[i];
        }
        for (int i = index + 1; i < count + 1; i++) {
            list[i] = temp[i-1];
        }
        list[index] = item;
        count++;
    }
    public int get(int index) {
        if (index < 0 || index > count - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        return list[index];
    }
    public int pop() {
        if (count == 0) {
            throw new RuntimeException("Index is invalid.");
        }
        int item;
        item = list[0];
        int[] temp;
        temp = new int[count - 1];
        count--;
        for (int i = 0; i < count; i++) {
            temp[i] = list[i + 1];
        }
        list = temp;
        return item;
    }
    public int getFirst() {
        if (count == 0) {
            throw new RuntimeException("Queue is clear.");
        }
        return list[0];
    }
    public void clear() {
        list = null;
        count = 0;
    }
    public boolean isEmpty() {
        return count == 0;
    }
    public int size() {
        return count;
    }
    public void replace(int index, int item){
        if (index < 0 || index > count) {
            throw new RuntimeException("Queue is clear.");
        }
        int[] temp;
        temp = list;
        list = new int[count];
        for (int i = 0; i < count; i++) {
            list[i] = temp[i];
        }
        list[index] = item;
    }
    public void delete(int index){
        if (index < 0 || index > count) {
            throw new RuntimeException("Queue is clear.");
        }
        int[] temp;
        temp = list;
        list = new int[count-1];
        //p = Arrays.copyOfRange(p2, 0, index);
        for (int i = 0; i < count - 1; i++){
            if (i < index){
                list[i] = temp[i];
            } else {
                list[i] = temp[i+1];
            }
        }
        count--;
    }
}
package List2;

public class List {
    private Node first;
    private Node last;
    public List() {
        first = null;
        last = null;
    }
    public boolean isEmpty() {
        return first == null;
    }
    public void pushBack(String value) {
        Node list = new Node(value);
        if (isEmpty()) {
            first = list;
            last = list;
            return;
        }
        last.setNext(list);
        last = list;
    }
    public int findSize() {
        int count = 0;
        Node current = first;
        while(current != last) {
            count++;
            current = current.getNext();
        }
        count++;
        return count;
    }
    public String get(int index) {
        Node current = first;
        for(int i=0; i<index; i++) {
            current = current.getNext();
        }
        return current.getValue();
    }
    public Node getNode(int index) {
        Node current = first;
        for(int i=0; i<index; i++) {
            current = current.getNext();
        }
        return current;
    }
    public void insert(int index, String item){
        Node newNode = new Node(item);
        Node current = first;
        for (int i = 1; i < index; i++){
            current = current.getNext();
        }
        newNode.setNext(current.getNext());
        current.setNext(newNode);
    }
    public void replace(int index, String item){
        Node newNode = this.getNode(index);
        newNode.setValue(item);
    }
    public void delete(int index){
        Node current = this.getNode(index-1);
        if (this.getNode(index)!= last) {
            Node next = current.getNext();
            current.setNext(next.getNext());
            next.setNext(null);
        } else {
            current.setNext(null);
            this.last = current;
        }
    }
}
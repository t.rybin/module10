package List2;

public class Main {
    public static void main(String[] args) {
        List list = new List();
        list.pushBack("one");
        list.pushBack("two");
        list.pushBack("three");
        list.pushBack("four");
        list.pushBack("five");
        list.insert(3, "insertion");
        list.replace(1, "replacement");
        list.delete(5);
        System.out.println(list.findSize());
        for(int i=0; i<list.findSize(); i++) {
            System.out.println(list.get(i));
        }
    }
}

